import matplotlib.pyplot as plt
import numpy as np
import os

EXP_FOLDER = '/small_undirected'

def process_file(quality_ax, time_ax, file_data, color, tag):
	f = open(os.getcwd() + EXP_FOLDER + '/' + file_data, 'r')
	xplot = []
	yplot = []
	tplot = []
	for line in f:
		a, b, c = line.strip('\n').split(' ')		
		xplot.append(a)
		yplot.append(b)
		tplot.append(c)
	quality_ax.plot(xplot, yplot, color, label=tag)
	time_ax.plot(xplot, tplot, color, label=tag)

def read_lazy_graph(lazy_ax, file_data):
    f = open(file_data, 'r')
    xplot = []
    yplot = []
    for line in f:
        a, b, c = line.rstrip('\n').split(' ')
        xplot.append(a)
        if int(b) == 0:
            yplot.append(1.0)
        else:
            yplot.append(1.0 * int(c)/int(b))
    lazy_ax.plot(xplot, yplot, 'b', label='greedy/lazy speedup')
    f.close()

if __name__ == "__main__":
    colors = ['r', 'b', 'g', 'y']
    quality_plot = plt.figure()
    quality_ax = quality_plot.add_subplot(111)
    quality_ax.set_xlabel('Seed set size (k)')
    quality_ax.set_ylabel('Target size')

    time_plot = plt.figure()
    time_ax = time_plot.add_subplot(111)
    time_ax.set_yscale('log')
    time_ax.set_xlabel('Seed set size (k)')
    time_ax.set_ylabel('Running time (sec)')
    for f, c in zip(os.listdir(os.getcwd() + EXP_FOLDER), colors):
	process_file(quality_ax, time_ax, f, c, f)

    quality_ax.legend()
    time_ax.legend()
        
    lazy_plot = plt.figure()
    lazy_ax = lazy_plot.add_subplot(111)
    lazy_ax.set_xlabel('Seed set size(k)')
    lazy_ax.set_ylabel('lazy/greedy ratio')
    read_lazy_graph(lazy_ax, 'greedy_ratio')
    lazy_ax.legend()
    plt.show()

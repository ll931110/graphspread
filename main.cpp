#include <algorithm>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <fstream>
#include <iostream>
#include <queue>
#include <set>
#include <vector>
#define NUM_VERTICES 350
#define NUM_SIMULATION 10000
using namespace std;

enum EdgeType {
	DIRECTED,
	UNDIRECTED
};

class Graph {
public:
    int numVertices;
    vector<int> neighbors[NUM_VERTICES];
};

class LiveGraph : public Graph {
public:
	LiveGraph() {
		numVertices = 0;
	};

	// Calculate how many vertices are reachable from the seed_set in a live graph
	// Which is the transitive closure problem
	// For undirected graph, it is simply counting the number of connected components
	// For directed graph, 
	int spreadValue(set<int> seedSet /*, vector<int> vertexCount, EdgeType et*/) {
		queue<int> potentialNodes;
		memset(reachable, false, sizeof reachable);

		for (auto v : seedSet) {
			potentialNodes.push(v);
			reachable[v] = true;
		}

		int spreadCount = 0;
		while (!potentialNodes.empty()) {
			int u = potentialNodes.front();
			spreadCount++;
			potentialNodes.pop();

			for (auto v : neighbors[u]) {
				if (!reachable[v]) {
					reachable[v] = true;
					potentialNodes.push(v);
				}
			}
		}

		return spreadCount;
	};

	~LiveGraph() {
	    for (int i = 0; i < numVertices; i++) {
	  		neighbors[i].clear();
	    }
	};

private:
	bool reachable[NUM_VERTICES];
};

class RandomGraph : public Graph {
public:
  RandomGraph(string filePath) {
  	  ifstream myFile;
	  myFile.open("data/web_proc.txt");
	  int n, m;
	  myFile >> n >> m;
	  numVertices = n;
	
	  for (int i = 0; i < n; i++) {
		  neighbors[i].clear();
	  }

	  for (int i = 0; i < m; i++) {
	  	  int x, y;
	  	  myFile >> x >> y;
	  	  neighbors[x].push_back(y);
	  	  neighbors[y].push_back(x);
	  }
	  myFile.close();
  }

  ~RandomGraph() {
      for (int i = 0; i < numVertices; i++) {
	      neighbors[i].clear();
	  }
  }

  LiveGraph generateLiveGraph(EdgeType et) {
  	  LiveGraph lg = LiveGraph();
      lg.numVertices = numVertices;
	  
	  for (int i = 0; i < numVertices; i++) {
		  lg.neighbors[i].clear();
	  }

	  switch (et) {
	  	case DIRECTED:
	  		for (int i = 0; i < numVertices; i++) {
		  		int deg = neighbors[i].size();
		  		for (auto v : neighbors[i]) {
			  		if (rand() % deg == 0) {
				  		lg.neighbors[i].push_back(v);
			  		}
		  		}
	  		}
	  		break;
	  	case UNDIRECTED:
	  		// Generate with fixed probability.
	  		// Keep eps = 0.1 now
	  		double eps = 0.1;
			for (int i = 0; i < numVertices; i++) {
				for (auto v : neighbors[i]) {
				  	if (i < v && rand() % 10 == 0) {
						lg.neighbors[i].push_back(v);
						lg.neighbors[v].push_back(i);
				  	}
				}
			}
	  		break;
	  }

	  return lg;
  }

  double spread_simulation(set<int> seedSet, EdgeType et) {
  	  int totalCount = 0;
  	  for (int i = 0; i < NUM_SIMULATION; i++) {
  		  LiveGraph lg = generateLiveGraph(et);
  		  totalCount += lg.spreadValue(seedSet);
  	  }
  	  return (double) totalCount / NUM_SIMULATION;
  }
};

void printResult(int k, double opt, clock_t begin) {
	clock_t end = clock();
	double elapsed_time = (double) (end - begin) / CLOCKS_PER_SEC;

	// Seed size, maximum spread and expected time
	printf("%d %.3lf %.3lf\n", k, opt, elapsed_time);
}

class Solution {
public:
	Solution(int seed_size) {
		this->seed_size = seed_size;		
		begin = clock();
		maxSize.resize(seed_size + 1);
		maxSet.resize(seed_size + 1);
		printResult(0, 0, begin);
	};

	~Solution() {
		maxSet.clear();
		maxSize.clear();
	}

	void greedy(RandomGraph rg, EdgeType et, bool IsLazyApplied) {
		if (IsLazyApplied) {
			init_lazy(rg.numVertices);
		}

		for (int i = 1; i <= seed_size; i++) {
			if (!IsLazyApplied) {
				for (int v = 0; v < rg.numVertices; v++) {
					if (maxSet[i - 1].find(v) == maxSet[i - 1].end()) {
						set<int> newSet = makeNewSet(maxSet[i - 1], v);
						double spreadVal = rg.spread_simulation(newSet, et);
						if (spreadVal > maxSize[i]) {
							maxSize[i] = spreadVal;
							maxSet[i] = newSet;
						}
					}
				}				
			} else {				
				int next_vertex;
				while (!lazy_pop(&next_vertex, i)) {
					set<int> newSet = makeNewSet(maxSet[i - 1], next_vertex);
					double spreadVal = rg.spread_simulation(newSet, et);
					spread_calc[next_vertex] = spreadVal - maxSize[i - 1];
					lazy_push(spread_calc[next_vertex], next_vertex);
				}
				maxSize[i] = spread_calc[next_vertex] + maxSize[i - 1];
				maxSet[i] = makeNewSet(maxSet[i - 1], next_vertex);
			}		
			printResult(i, maxSize[i], begin);
		}
	}

	void stochasticGreedy(RandomGraph rg, EdgeType et, double eps) {
		init_lazy(rg.numVertices);
		int sample_size = (int) floor(rg.numVertices / seed_size * log(1/eps));
		for (int i = 1; i <= seed_size; i++) {					
			vector<int> include_set;
			for (int j = 0; j < rg.numVertices; j++) {
				if (maxSet[i - 1].find(j) == maxSet[i - 1].end()) {
					if (rand() % (rg.numVertices - i + 1) < sample_size) {
						include_set.push_back(j);
					}
				}
			}

			for (auto v : include_set) {
				set<int> newSet = makeNewSet(maxSet[i - 1], v);
				double spread = rg.spread_simulation(newSet, et);
				if (spread > maxSize[i]) {
					maxSize[i] = spread;
					maxSet[i] = newSet;
				}
			}
			printResult(i, maxSize[i], begin);
		}
	}

	void backoffGreedy(RandomGraph rg, EdgeType et, double alpha) {
		init_lazy(rg.numVertices);
		for (int i = 1; i <= seed_size; i++) {
			double cutoff = 0;
			for (int v = 0; v < rg.numVertices; v++) if (maxSet[i - 1].find(v) == maxSet[i - 1].end()) {
				if (cutoff < spread_calc[v]) {
					cutoff = spread_calc[v];
				}					
			}
			if (cutoff <= 0) {
				cutoff = rg.numVertices;
			}
			cutoff *= alpha;

			for (int v = 0; v < rg.numVertices; v++) if (maxSet[i - 1].find(v) == maxSet[i - 1].end()) {
				set<int> newSet = makeNewSet(maxSet[i - 1], v);
				double spread = rg.spread_simulation(newSet, et);
				if (spread > maxSize[i]) {
					maxSize[i] = spread;
					maxSet[i] = newSet;
				}
				spread_calc[v] = spread - maxSize[i - 1];
				if (spread_calc[v] >= cutoff) {
					break;
				}
			}
			printResult(i, maxSize[i], begin);
		}
	}

private:
	clock_t begin;
	int seed_size;
	vector<double> maxSize;
	vector< set<int> > maxSet;

	// For lazy propagation
	vector<int> valid_assign;
	vector<double> spread_calc;
	priority_queue< pair<double, int> > pq;

	set<int> makeNewSet(set<int> old, int elt) {
		set<int> returnSet = old;
		returnSet.insert(elt);
		return returnSet;
	}

	void init_lazy(int numVertices) {
		valid_assign.resize(numVertices);
		spread_calc.resize(numVertices);
		for (int i = 0; i < numVertices; i++) {
			valid_assign[i] = 0;
			spread_calc[i] = 0;
			pq.push(make_pair(numVertices, i));
		}
	}

	// Return true if queue is empty, or we won't put this vertex into queue anymore (and terminate caller's call)
	bool lazy_pop(int * u, int idx) {
		if (pq.empty()) {
			*u = 0;
			return true;
		}
		*u = pq.top().second;
		pq.pop();
		if (valid_assign[*u] < idx) {
			valid_assign[*u] = idx;
			return false;
		}
		return true;
	}

	void lazy_push(double value, int u) {
		pq.push(make_pair(value, u));
	}
};


int main(int argc, char * argv[]) {
	const string USAGE = "Usage: -s [integer] -m [method] -f [filename] -d [0/1]\n";
    if (argc != 9) {
    	cout << USAGE << endl;
    	return 0;
    }

    int seed_size;
    int directed;
    string method;
    string filename;    
    for (int i = 1; i < argc; i += 2) {
    	if (strcmp(argv[i], "-s") == 0) {    		
    		seed_size = atoi(argv[i + 1]);
    	}
    	else if (strcmp(argv[i], "-m") == 0) {
    		method = string(argv[i + 1]);
    	}
    	else if (strcmp(argv[i], "-f") == 0) {
    		filename = string(argv[i + 1]);
    	}
    	else if (strcmp(argv[i], "-d") == 0) {
    		directed = atoi(argv[i + 1]);
    	}
    	else {
    	    cout << USAGE << endl;
    		return 0;    		
    	}
    }
    EdgeType et = (directed == 0) ? UNDIRECTED : DIRECTED;

    RandomGraph rg = RandomGraph(filename);
    Solution sln = Solution(seed_size);  // seed_size is between 1 and 20
    if (method == "orig") {
    	sln.greedy(rg, et, false);
    }
    else if (method == "lazy") {
    	sln.greedy(rg, et, true);    	
    }
    else if (method == "stoc") {
    	sln.stochasticGreedy(rg, et, 0.01);
    }
    else if (method == "back") {
    	sln.backoffGreedy(rg, et, 0.5);
    }
    else {
    	cout << USAGE << endl;
    }
    return 0;
}